#Uso del modelo
import pandas as pd
import re
from tqdm.notebook import tqdm
import numpy as np
from torch.utils.data import TensorDataset
import torch
from torch.utils.data import DataLoader, RandomSampler, SequentialSampler
from transformers import BertForSequenceClassification
from transformers import BertTokenizer

def read_file(file):
  return pd.read_csv(file, encoding = "ISO-8859-1")

def clean_text(text):
  text = str(text)
  text = ' '.join(re.sub(u'[^a-zA-Z0-9áéíóúÁÉÍÓÚâêîôÂÊÎÔãõÃÕçÇñÑ]', ' ', text.replace('\n', ' ').replace('\r', '')).split()).lower()
  return text

def clean_data(df, field_name):
  df[field_name] = df[field_name].apply(clean_text)

def tokenize_text(df,field_name):
  tokenizer = BertTokenizer.from_pretrained('dccuchile/bert-base-spanish-wwm-uncased')
  encoded_data = tokenizer.batch_encode_plus(
    df[field_name].values,
    add_special_tokens = True,
    return_attention_mask = True,
    max_length=256,
    truncation=True,
    padding=True,
    return_tensors='pt'
    )

  input_ids = encoded_data['input_ids']
  attention_masks = encoded_data['attention_mask']
  dataset = TensorDataset(input_ids,
                              attention_masks)
  return dataset

def create_dataloader(dataset):
  batch_size = 32
  dataloader = DataLoader(
    dataset,
    sampler=RandomSampler(dataset),
    batch_size=batch_size
  )
  return dataloader

def get_predictions(dl, model_link, label_dict):
  model = BertForSequenceClassification.from_pretrained("dccuchile/bert-base-spanish-wwm-uncased",
                                                      num_labels=len(label_dict))
  device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
  model.to(device)
  model.load_state_dict(torch.load(model_link, map_location=torch.device(device)))
  model.eval()
  predictions = []

  for batch in tqdm(dl):
      batch = tuple(b.to(device) for b in batch)
      inputs = {'input_ids':      batch[0],
                'attention_mask': batch[1]
                }
      with torch.no_grad():        
          outputs = model(**inputs)  
      logits = outputs[0]  
      logits = logits.detach().cpu().numpy()
      predictions.append(logits)
  
  predictions = np.concatenate(predictions, axis=0)
  preds_flat = np.argmax(predictions, axis=1).flatten()
  return preds_flat

def add_predictions(df, predictions, l_dict):
  label_dict = l_dict
  label_dict_inverse = {v: k for k,v in label_dict.items()}
  df['prediction'] = predictions
  df['prediction'] = df.prediction.apply(lambda x: label_dict_inverse[x])

def use_model(filename, model, label_dict, field_name):
  df = read_file(filename)
  df_raw = df
  print("Se leyo la data")
  clean_data(df, field_name)
  print("Se limpio la data")
  dataset = tokenize_text(df, field_name)
  print("Se tokenizo la data")
  dl = create_dataloader(dataset)
  print("Se creo el dataloader")
  predictions = get_predictions(dl, model, label_dict)
  print("Se realizo la prediccion")
  add_predictions(df_raw, predictions, label_dict)
  print("Se genero el df final")

  return df_raw

def use_model_ind(text, model_link, label_dict):

  text = clean_text(text)
  tokenizer = BertTokenizer.from_pretrained('dccuchile/bert-base-spanish-wwm-uncased')
  encoded_data = tokenizer.batch_encode_plus(
    [text],
    add_special_tokens = True,
    return_attention_mask = True,
    max_length=256,
    truncation=True,
    padding=True,
    return_tensors='pt'
    )

  input_ids = encoded_data['input_ids']
  attention_masks = encoded_data['attention_mask']
  dataset = TensorDataset(input_ids,
                              attention_masks)
  batch_size = 32
  dataloader = DataLoader(
    dataset,
    sampler=RandomSampler(dataset),
    batch_size=batch_size
  )

  model = BertForSequenceClassification.from_pretrained("dccuchile/bert-base-spanish-wwm-uncased",
                                                        num_labels=len(label_dict))
  device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
  model.to(device)
  model.load_state_dict(torch.load(model_link, map_location=torch.device(device)))
  model.eval()
  predictions = []

  for batch in tqdm(dataloader):
      batch = tuple(b.to(device) for b in batch)
      inputs = {'input_ids':      batch[0],
                'attention_mask': batch[1]
                }
      with torch.no_grad():        
          outputs = model(**inputs)  
      logits = outputs[0]  
      logits = logits.detach().cpu().numpy()
      predictions.append(logits)

  predictions = np.concatenate(predictions, axis=0)
  preds_flat = np.argmax(predictions, axis=1).flatten()
  label_dict = LABEL_DICT = {'N': 2, 'NEU': 3, 'NONE': 0, 'P': 1}
  label_dict_inverse = {v: k for k,v in label_dict.items()}
  pred =  label_dict_inverse[preds_flat[0]]
  
  return pred