from flask_restful import Resource, reqparse
from flask import jsonify, request, send_from_directory

from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

import json

# HEIL FILESYSTEM
import os
from pathlib import Path
from datetime import datetime
from application.api import app

#Uso del modelo
from application.resources import functions

parser = reqparse.RequestParser()
parser.add_argument('file', type=FileStorage, help='Not Blank', required=False, location='files')
ALLOWED_EXTENSIONS = ['csv', 'xlsx','txt']
LABEL_DICT = {'N': 2, 'NEU': 3, 'NONE': 0, 'P': 1}
MODEL = 'application/models/Beto_2_Adam_ft_epoch17.model'

class PredictTweets(Resource):
    def post(self):
        data = parser.parse_args()
        if data['file']: 
            file = data['file']
            filename = secure_filename(file.filename)
            print ("Predict Tweets")
            print ("----------------------")
            print ("Nombre del archivo: "+ filename)
            print (type(file))
            full_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(full_path)

            print(full_path)
            predictions = functions.use_model(full_path, MODEL, LABEL_DICT,'tweet')
            print("Se realizo la prediccion")

            now = datetime.now()
            dt_string = now.strftime("%d%m%Y_%H%M%S")
            filename_out = "social_media_prediction"+dt_string+".csv"
            predictions.to_csv("application/tempfiles/"+filename_out, encoding = "ISO-8859-1", index=False)
            #return send_from_directory(app.config['UPLOAD_FOLDER'],
            #                   filename_out, as_attachment=True)
            print("Archivo Generado: "+filename_out)
        
        return jsonify(status="Correcto")