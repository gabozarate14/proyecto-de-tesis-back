from flask_restful import Resource, reqparse
from flask import jsonify, request, send_from_directory

from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

import json

# HEIL FILESYSTEM
import os
from pathlib import Path
from datetime import datetime
from application.api import app

#Uso del modelo
from application.resources import functions

parser = reqparse.RequestParser()
parser.add_argument('textInput', help='Not Blank', required=True)
LABEL_DICT = {'Negativo': 2, 'Neutro': 1, 'Positivo': 0}
MODEL = 'application/models/Beto_News_Ada_3_ft_epoch8.model'

class PredictArticle(Resource):
    def post(self):
        data = parser.parse_args()
        text = data['textInput']
        print ("Predict Article")
        print ("----------------------")
        classification = functions.use_model_ind(text, MODEL, LABEL_DICT)
        print("Se realizo la prediccion")
        
        return jsonify(status="Correcto", clasification = classification)