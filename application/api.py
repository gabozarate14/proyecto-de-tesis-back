from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy, event
from flask_cors import CORS
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from flask import Flask

UPLOAD_FOLDER = 'application\\tempfiles'

# API CONFIG
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
api = Api(app)
CORS(app)

# SERVICIOS GENERALES
from application.services.general_services import predict_tweets
from application.services.general_services import predict_articles
from application.services.general_services import predict_tweet
from application.services.general_services import predict_article

# API RESOURCES 
api.add_resource(predict_tweets.PredictTweets, '/predict_tweets')
api.add_resource(predict_articles.PredictArticles, '/predict_articles')
api.add_resource(predict_tweet.PredictTweet, '/predict_tweet')
api.add_resource(predict_article.PredictArticle, '/predict_article')
