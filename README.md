# Web de pruebas de modelo de análisis de sentimiento

Proyecto académico Tesis 2

### Prerequisites

- Python 3.*  

### Installing

Para correr el Backend realizar los siguientes pasos

1. Instalar todas las dependencias en un virtualenv:

```
$ pip install -U -r requirements.txt
```

2. Descargar los modelos entrenados del siguiente enlace y colocarlos en application\models https://drive.google.com/drive/folders/1iN8Dbl2Bm9UBeHNugPBnZA-ZHGCJ36oh?usp=sharing


3. Correr run.py 

```
$ python run.py
```

